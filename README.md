# Mini SALR

## What does it do?

 - It highlights threads that you've seen and may or may not have new posts without being annoying
 - It gives you a nice link at the top of the User Control Panel to open all your bookmarks that have new posts
  - It will also close the User Control Panel tab if it can
 - No configuration! Woo!

## What doesn't it do?

 - Everything else that other SA Forum plugins do

## Usage

 - For Opera, install `salr.nex`
 - For Chrome, install `salr.crx`
 - For GreaseMonkey or TamperMonkey, install as a [userscript](https://greasyfork.org/en/scripts/15667-mini-salr)

## License
[GPL](LICENSE)
